﻿using UnityEngine;

namespace Assets.Content.Scripts.SystemLogick
{
  [RequireComponent(typeof(AudioSource))]
  public class AudioManager: MonoBehaviour
  {
    #region Private Members
    [SerializeField]
    private AudioSource _tankAudioSource;
    [SerializeField]
    private AudioSource _sfxAudioSource;
    [SerializeField]
    private AudioSource _musicAudioSource;
    [SerializeField]
    private AudioClip _tankIdlingAudioClip;
    [SerializeField]
    private AudioClip _tankMoveAudioClip;
    [SerializeField]
    private AudioClip _tankReloadAudioClip;
    [SerializeField]
    private AudioClip _tankShotAudioClip;
    [SerializeField]
    private AudioClip _tankExplosionAudioClip;
    [SerializeField]
    private AudioClip _bulletExplosionAudioClip;
    [SerializeField]
    private AudioClip _preloaderMusicAudioClip;
    [SerializeField]
    private AudioClip _gameMusicAudioClip;
    private bool _isMove = false;
    private bool _isRotate = false;
    #endregion

    #region Public Methods
    public void Initialize()
    {
      DontDestroyOnLoad(gameObject);

      if (!ErrorFinded())
      {
        _tankAudioSource = GetComponent<AudioSource>();
        _tankAudioSource.Stop();
        ApplicationContainer.Instance.EventHolder.SetPawnMoveDirectionEvent += PlayPawnMoveSound;
        ApplicationContainer.Instance.EventHolder.SetPawnActionEvent += PlayPawnActionSound;
        ApplicationContainer.Instance.EventHolder.PlayMusicEvent += PlayMusic;
        ApplicationContainer.Instance.EventHolder.StopMusicEvent += StopMusic;
      }
    }

    public void PlayeBulletExplosionSount()
    {
      _sfxAudioSource.clip = _bulletExplosionAudioClip;
      _sfxAudioSource.Play();
    }

    public void PlayeTankExplosionSount()
    {
      _sfxAudioSource.clip = _tankExplosionAudioClip;
      _sfxAudioSource.Play();
    }

    public void StopTankSound()
    {
      _tankAudioSource.Stop();
    }
    #endregion

    #region Private Methods
    private void PlayPawnMoveSound(Core.Pawn.PawnMoveDirection direction)
    {
      if (!ApplicationContainer.Instance.GameFieldController.TankPawn.IsDead)
      {
        _tankAudioSource.clip = _tankIdlingAudioClip;

        switch (direction)
        {
          case Core.Pawn.PawnMoveDirection.MoveForward:
          case Core.Pawn.PawnMoveDirection.MoveBack:
            _isMove = true;
            break;
          case Core.Pawn.PawnMoveDirection.RotateLeft:
          case Core.Pawn.PawnMoveDirection.RotateRight:
            _isRotate = true;
            break;
          case Core.Pawn.PawnMoveDirection.StopMove:
            _isMove = false;
            break;
          case Core.Pawn.PawnMoveDirection.StopRotate:
            _isRotate = false;
            break;
        }

        if (_isMove || _isRotate)
        {
          _tankAudioSource.clip = _tankMoveAudioClip;
        }

        _tankAudioSource.Play();
      }
    }

    private void PlayPawnActionSound(Core.Pawn.PawnAction action)
    {
      if (!ApplicationContainer.Instance.GameFieldController.TankPawn.IsDead)
      {
        switch (action)
        {
          case Core.Pawn.PawnAction.NextWeapon:
          case Core.Pawn.PawnAction.PreviousWeapon:
            _sfxAudioSource.clip = _tankReloadAudioClip;
            break;
          case Core.Pawn.PawnAction.Shoot:
            _sfxAudioSource.clip = _tankShotAudioClip;
            break;
        }

        _sfxAudioSource.Play();
      }
    }

    private void PlayMusic(MusicName name)
    {
      switch (name)
      {
        case MusicName.Preloader:
          _musicAudioSource.clip = _preloaderMusicAudioClip;
          _musicAudioSource.loop = false;
          break;
        case MusicName.Game:
          _musicAudioSource.clip = _gameMusicAudioClip;
          _musicAudioSource.loop = true;
          break;
      }

      _musicAudioSource.Play();
    }

    private void StopMusic()
    {
      _musicAudioSource.Stop();
    }

    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_tankAudioSource == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankAudioSource is empty", GetType().Name));
        returnValue = true;
      }

      if (_sfxAudioSource == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_sfxAudioSource is empty", GetType().Name));
        returnValue = true;
      }

      if (_musicAudioSource == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_musicAudioSource is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankIdlingAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankIdlingAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankMoveAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankMoveAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankReloadAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankReloadAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankShotAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankShotAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankExplosionAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankExplosionAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_bulletExplosionAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_bulletExplosionAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_preloaderMusicAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_preloaderMusicAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      if (_gameMusicAudioClip == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_gameMusicAudioClip is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }
    #endregion
  }
}