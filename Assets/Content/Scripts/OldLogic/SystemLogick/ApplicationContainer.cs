﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using Assets.Content.Scripts.UI;
using Assets.Content.Scripts.Core.PlayerInput;
using Assets.Content.Scripts.Core.GameField;

namespace Assets.Content.Scripts.SystemLogick
{
  public class ApplicationContainer: Singleton<ApplicationContainer>
  {
    #region Private Members
    [SerializeField]
    private GameObject _uiPrefab;
    [SerializeField]
    private GameObject _gameFieldPrefab;
    [SerializeField]
    private GameObject _audioManagerPrefab;
    private LevelName _loadedLevel;
    private bool _isLevelObjectsLoaded = false;
    #endregion

    #region Properties 
    public EventHolder EventHolder { get; private set; }
    public DataBaseHolder DataBaseHolder { get; private set; }
    public UIController UIController { get; private set; }
    public GameFieldController GameFieldController { get; private set; }
    public InputController InputController { get; private set; }
    public AudioManager AudioManager { get; private set; }
    public bool IsPreloaderAnimationFinished { get; set; }
    #endregion

    #region Public Methods
    public void LoadLevel(LevelName name)
    {
      _loadedLevel = name;
      EventHolder.HidePanel();
      EventHolder.ShowPanel(PanelType.Preloader);

      if (name == LevelName.GameScene)
      {
        ApplicationContainer.Instance.EventHolder.PlayMusic(MusicName.Preloader);
      }
      else
      {
        ApplicationContainer.Instance.EventHolder.StopMusic();
      }

      StartCoroutine(WaitSceneLoaded());
      SceneManager.sceneLoaded += OnSceneLoaded;
      SceneManager.LoadScene(name.ToString());
    }
    #endregion

    #region Private Methods
    private void Start()
    {
      DontDestroyOnLoad(gameObject);
      SceneManager.sceneLoaded += OnFirstSceneLoaded;
      SceneManager.LoadScene(LevelName.MainMenu.ToString());
    }

    private void OnFirstSceneLoaded(Scene scene, LoadSceneMode mode)
    {
      SceneManager.sceneLoaded -= OnFirstSceneLoaded;

      if (!ErrorFinded())
      {
        FirstInitialize();
        EventHolder.Initialize();
        AudioManager.Initialize();
        UIController.Initialize();
      }
    }

    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_uiPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_uiPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_gameFieldPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_gameFieldPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_audioManagerPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_audioManagerPrefab is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }

    private void FirstInitialize()
    {
      if (EventHolder == null)
      {
        EventHolder = CreateObject<EventHolder>();
      }
      if (DataBaseHolder == null)
      {
        DataBaseHolder = CreateObject<DataBaseHolder>();
      }
      if (AudioManager == null)
      {
        AudioManager = Instantiate(_audioManagerPrefab).GetComponent<AudioManager>();
      }
      if (UIController == null)
      {
        UIController = Instantiate(_uiPrefab).GetComponent<UIController>();
      }
    }

    private T CreateObject<T>() where T : class
    {
      return new GameObject(typeof(T).Name, typeof(T)).GetComponent<T>();
    }

    private void GameLogickInitialize()
    {
      GameFieldController = Instantiate(_gameFieldPrefab).GetComponent<GameFieldController>();
      InputController = CreateObject<InputController>();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
      SceneManager.sceneLoaded -= OnSceneLoaded;

      if (scene.name == LevelName.GameScene.ToString())
      {
        GameLogickInitialize();
        GameFieldController.Initialize();
        InputController.Initialize();
      }
      else if (scene.name == LevelName.MainMenu.ToString())
      {
      }

      _isLevelObjectsLoaded = true;
    }

    private IEnumerator WaitSceneLoaded()
    {
      bool exit = false;

      while (!exit)
      {
        if (IsPreloaderAnimationFinished && _isLevelObjectsLoaded)
        {
          _isLevelObjectsLoaded = false;
          IsPreloaderAnimationFinished = false;
          exit = true;
        }

        yield return null;
      }

      switch (_loadedLevel)
      {
        case LevelName.GameScene:
          EventHolder.HidePanel();
          EventHolder.ShowPanel(PanelType.GamePanel);
          ApplicationContainer.Instance.EventHolder.PlayMusic(MusicName.Game);
          break;
        case LevelName.MainMenu:
          EventHolder.HidePanel();
          EventHolder.ShowPanel(PanelType.MainMenu);
          break;
      }

      _loadedLevel = LevelName.Default;
    }
    #endregion
  }
}