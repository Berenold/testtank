﻿using UnityEngine;
using System;
using Assets.Content.Scripts.UI;
using Assets.Content.Scripts.Core.Pawn;

namespace Assets.Content.Scripts.SystemLogick
{
  public class EventHolder: MonoBehaviour
  {
    public void Initialize()
    {
      DontDestroyOnLoad(gameObject);
    }

    #region UI
    public Action<PanelType> ShowPanelEvent;
    public void ShowPanel(PanelType currentPanel)
    {
      if (ShowPanelEvent != null)
      {
        ShowPanelEvent(currentPanel);
      }
    }

    public Action HidePanelEvent;
    public void HidePanel()
    {
      if (HidePanelEvent != null)
      {
        HidePanelEvent();
      }
    }

    public Action ReloadPlayerHealthIndicatorEvent;
    public void ReloadPlayerHealthIndicator()
    {
      if (ReloadPlayerHealthIndicatorEvent != null)
      {
        ReloadPlayerHealthIndicatorEvent();
      }
    }

    public Action ShowRespawnTimeEvent;
    public void ShowRespawnTime()
    {
      if (ShowRespawnTimeEvent != null)
      {
        ShowRespawnTimeEvent();
      }
    }

    public Action ChangeLifeCountEvent;
    public void ChangeLifeCount()
    {
      if (ChangeLifeCountEvent != null)
      {
        ChangeLifeCountEvent();
      }
    }

    public Action ShowGameOverEvent;
    public void ShowGameOver()
    {
      if (ShowGameOverEvent != null)
      {
        ShowGameOverEvent();
      }
    }

    public Action UpdateScoreEvent;
    public void UpdateScore()
    {
      if (UpdateScoreEvent != null)
      {
        UpdateScoreEvent();
      }
    }
    #endregion

    #region Pawn
    public Action<PawnMoveDirection> SetPawnMoveDirectionEvent;
    public void SetPawnMoveDirection(PawnMoveDirection moveDirection)
    {
      if (SetPawnMoveDirectionEvent != null)
      {
        SetPawnMoveDirectionEvent(moveDirection);
      }
    }

    public Action<PawnAction> SetPawnActionEvent;
    public void SetPawnAction(PawnAction pawnAction)
    {
      if (SetPawnActionEvent != null)
      {
        SetPawnActionEvent(pawnAction);
      }
    }
    #endregion

    #region Input
    public Action<bool> ChangeInputTrackingStatusEvent;
    public void ChangeInputTrackingStatus(bool isTracking)
    {
      if (ChangeInputTrackingStatusEvent != null)
      {
        ChangeInputTrackingStatusEvent(isTracking);
      }
    }
    #endregion

    #region Music
    public Action<MusicName> PlayMusicEvent;
    public void PlayMusic(MusicName name)
    {
      if (PlayMusicEvent != null)
      {
        PlayMusicEvent(name);
      }
    }

    public Action StopMusicEvent;
    public void StopMusic()
    {
      if (StopMusicEvent != null)
      {
        StopMusicEvent();
      }
    }
    #endregion
  }
}