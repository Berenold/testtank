namespace Assets.Content.Scripts.SystemLogick
{
  public enum LevelName
  {
    Splash,
    MainMenu,
    GameScene,
    Default
  }
}