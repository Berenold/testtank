﻿using UnityEngine;
using Assets.Content.Scripts.Core.Pawn;
using Assets.Content.Scripts.Core.CameraContol;
using Assets.Content.Scripts.SystemLogick;
using System.Collections;
using System.Collections.Generic;

namespace Assets.Content.Scripts.Core.GameField
{
  public class GameFieldController: MonoBehaviour
  {
    #region Private Members
    [SerializeField]
    private GameObject _enemyPawnPrefab;
    [SerializeField]
    private GameObject _tankPawnPrefab;
    [SerializeField]
    private GameObject _cameraControllerPrefab;
    [SerializeField]
    private int _maxEnemyCount = 10;
    [SerializeField]
    private EnemyEssence[] _enemyEssence;
    private GameObject[] _playerSpawnPoints;
    private GameObject[] _enemySpawnPoints;
    private GameObject _deadPoint;
    private float _enemySpawnTime = 2;
    private bool _isEnemyRespawn = false;
    #endregion

    #region Properties
    public TankPawn TankPawn { get; private set; }
    public CameraController CameraController { get; private set; }
    public List<EnemyPawn> EnemyPool { get; set; }
    public int Score { get; private set; }
    #endregion

    #region Public Methods
    public void Initialize()
    {
      EnemyPool = new List<EnemyPawn>();

      if (!ErrorFinded())
      {
        _playerSpawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawnPoint");
        _enemySpawnPoints = GameObject.FindGameObjectsWithTag("EnemySpawnPoint");
        _deadPoint = GameObject.FindGameObjectWithTag("DeadPoint");

        if (_deadPoint == null)
        {
          Debug.LogError(string.Format("[{0}][Initialize]_deadPoint is empty", GetType().Name));
        }
        InitializeObjects();

        if (_playerSpawnPoints != null)
        {
          int playerSpawnPointIndex = Random.Range(0, _playerSpawnPoints.Length);
          TankPawn.transform.position = _playerSpawnPoints[playerSpawnPointIndex].transform.position;
          TankPawn.transform.rotation = _playerSpawnPoints[playerSpawnPointIndex].transform.rotation;
          TankPawn.SetPawn(100, 20, 5, 120, new int[2] { 100, 1000 });
          TankPawn.Initialize();
          CameraController.Initialize(TankPawn.transform);
        }
        else
        {
          Debug.LogError(string.Format("[{0}][Initialize]_playerSpawnPoints is empty", GetType().Name));
        }

        if (_enemySpawnPoints != null)
        {
          for (int i = 0; i < _maxEnemyCount; i++)
          {
            int enemySpawnPointIndex = Random.Range(0, _playerSpawnPoints.Length);
            EnemyPawn newEnemy = Instantiate(_enemyPawnPrefab).GetComponent<EnemyPawn>();
            newEnemy.transform.position = _enemySpawnPoints[enemySpawnPointIndex].transform.position;
            newEnemy.transform.rotation = _enemySpawnPoints[enemySpawnPointIndex].transform.rotation;
            newEnemy.Initialize();
            int enemyEssenceIndex = Random.Range(0, _enemyEssence.Length);
            newEnemy.SetPawn(TankPawn.transform,
                             _enemyEssence[enemyEssenceIndex].Damage,
                             _enemyEssence[enemyEssenceIndex].MaxHealth,
                             _enemyEssence[enemyEssenceIndex].Material,
                             _deadPoint.transform.position);
            newEnemy.ChangePawnMoveStatus(true);
          }
        }
        else
        {
          Debug.LogError(string.Format("[{0}][Initialize]_enemySpawnPoints is empty", GetType().Name));
        }
      }
    }

    public void RespawnTankPawn()
    {
      int playerSpawnPointIndex = Random.Range(0, _playerSpawnPoints.Length);
      TankPawn.transform.position = _playerSpawnPoints[playerSpawnPointIndex].transform.position;
      TankPawn.transform.rotation = _playerSpawnPoints[playerSpawnPointIndex].transform.rotation;
      TankPawn.Respawn();
      ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Core.Pawn.PawnMoveDirection.StopMove);
      CameraController.Initialize(TankPawn.transform);
    }

    public void AddScore(int score)
    {
      Score += score;
      ApplicationContainer.Instance.EventHolder.UpdateScore();
    }

    public void RespawnEnemy()
    {
      if (!_isEnemyRespawn)
      {
        _isEnemyRespawn = true;
        StartCoroutine("EnemyRespawn");
      }
    }
    #endregion

    #region Private Methods
    private IEnumerator EnemyRespawn()
    {
      bool exit = false;
      float timeShift = 0;

      while (!exit)
      {
        if (EnemyPool.Count > 0)
        {
          timeShift += Time.deltaTime;

          if (timeShift >= _enemySpawnTime)
          {
            int enemySpawnPointIndex = Random.Range(0, _enemySpawnPoints.Length);
            EnemyPool[0].transform.position = _enemySpawnPoints[enemySpawnPointIndex].transform.position;
            EnemyPool[0].transform.rotation = _enemySpawnPoints[enemySpawnPointIndex].transform.rotation;
            EnemyPool[0].Respawn();
            EnemyPool.RemoveAt(0);
            timeShift = 0;
          }
        }
        else
        {
          exit = true;
        }

        yield return null;
      }

      _isEnemyRespawn = false;
      StopCoroutine("EnemyRespawn");
    }

    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_tankPawnPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankPawnPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_cameraControllerPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_cameraControllerPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_enemyPawnPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_enemyPawnPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_enemyEssence == null || _enemyEssence.Length == 0)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_enemyEssence is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }

    private void InitializeObjects()
    {
      TankPawn = Instantiate(_tankPawnPrefab).GetComponent<TankPawn>();
      CameraController = Instantiate(_cameraControllerPrefab).GetComponent<CameraController>();
    }
    #endregion
  }
}