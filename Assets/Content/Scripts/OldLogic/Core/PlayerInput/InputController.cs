﻿using UnityEngine;
using System.Collections;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.Core.PlayerInput
{
  public class InputController: MonoBehaviour
  {
    #region Public Methods
    public void Initialize()
    {
      ApplicationContainer.Instance.EventHolder.ChangeInputTrackingStatusEvent += ChangeInputTrackingStatus;
      ChangeInputTrackingStatus(true);
    }

    public void ChangeInputTrackingStatus(bool isTracking)
    {
      if (isTracking)
      {
        StartCoroutine(InputTracking());
      }
      else
      {
        StopCoroutine(InputTracking());
      }
    }
    #endregion

    #region Private Methods
    private IEnumerator InputTracking()
    {
      while (true)
      {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
          ApplicationContainer.Instance.GameFieldController.TankPawn.TakeDamage(70);
        }
        #endif
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.RotateLeft);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.RotateRight);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow) ||
           Input.GetKeyUp(KeyCode.RightArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.StopRotate);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.MoveBack);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.MoveForward);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow) ||
           Input.GetKeyUp(KeyCode.UpArrow))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Pawn.PawnMoveDirection.StopMove);
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnAction(Pawn.PawnAction.PreviousWeapon);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnAction(Pawn.PawnAction.NextWeapon);
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
          ApplicationContainer.Instance.EventHolder.SetPawnAction(Pawn.PawnAction.Shoot);
        }

        yield return null;
      }
    }
    #endregion
  }
}