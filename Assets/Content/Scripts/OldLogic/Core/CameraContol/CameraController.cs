﻿using UnityEngine;
using System.Collections;

namespace Assets.Content.Scripts.Core.CameraContol
{
  public class CameraController: MonoBehaviour
  {
    #region Private Members
    [SerializeField]
    private Transform _target;
    #endregion

    #region Public Methods
    public void Initialize(Transform target)
    {
      if (!target)
      {
        Debug.LogError(string.Format("[{0}][Initialize]target is empty", GetType().Name));
        return;
      }

      _target = target;
      ChangeTrackingStatus(true);
    }

    public void ChangeTrackingStatus(bool startTracking)
    {
      if (startTracking)
      {
        StartCoroutine("Tracking");
      }
      else
      {
        StopCoroutine("Tracking");
      }
    }
    #endregion

    #region Private Methods
    private IEnumerator Tracking()
    {
      while (true)
      {
        var moveVelocity = new Vector3();
        transform.position = Vector3.SmoothDamp(transform.position, _target.position, ref moveVelocity, 0);
        yield return null;
      }
    }
    #endregion
  }
}