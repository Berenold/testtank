﻿namespace Assets.Content.Scripts.Core.Pawn
{
  public enum PawnMoveDirection
  {
    MoveForward,
    MoveBack,
    RotateLeft,
    RotateRight,
    StopMove,
    StopRotate,
    Default
  }
}