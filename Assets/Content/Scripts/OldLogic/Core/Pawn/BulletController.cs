﻿using UnityEngine;
using System.Collections;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.Core.Pawn
{
  [RequireComponent(typeof(Rigidbody))]
  public class BulletController: MonoBehaviour
  {
    #region Private Members
    [SerializeField]
    private ParticleSystem _explosionParticles;
    [SerializeField]
    private float _speed = 15f;
    private int _bulletDamage;
    private Rigidbody _rigidbody;
    private bool _move;
    #endregion

    #region Public Methods
    public void Initialize(int bulletDamage)
    {
      if (!ErrorFinder())
      {
        _rigidbody = GetComponent<Rigidbody>();
        _bulletDamage = bulletDamage;
        _move = true;
        StartCoroutine(BulletMovement());
      }
      else
      {
        Destroy(gameObject);
      }
    }
    #endregion

    #region Private Methods
    private bool ErrorFinder()
    {

      if (_explosionParticles)
      {
        return false;
      }

      Debug.LogErrorFormat("[{0}][ErrorFinder]_explosionParticles is empty", GetType().Name);
      return true;
    }

    private IEnumerator BulletMovement()
    {
      while (true)
      {
        if (_move)
        {
          _rigidbody.velocity = transform.TransformDirection(new Vector3(0, 0, _speed));
        }

        yield return null;
      }
    }

    private void OnTriggerEnter(Collider collider)
    {
      _move = false;
      StopCoroutine("BulletMovement");
      _rigidbody.velocity = transform.TransformDirection(Vector3.zero);
      ApplicationContainer.Instance.AudioManager.PlayeBulletExplosionSount();
      DoDamage(collider);
      var explosion = Instantiate(_explosionParticles.gameObject).GetComponent<ParticleSystem>();
      explosion.transform.position = transform.position;
      explosion.Play();
      var mainModule = explosion.main;
      Destroy(explosion.gameObject, mainModule.duration);
      ApplicationContainer.Instance.GameFieldController.TankPawn.BulletPool.Add(this);
      transform.position = ApplicationContainer.Instance.GameFieldController.TankPawn.BulletWaitPoint;
      gameObject.SetActive(false);
    }

    private void DoDamage(Collider collider)
    {
      var enemyPawn = collider.GetComponent<EnemyPawn>();

      if (enemyPawn)
      {
        enemyPawn.TakeDamage(_bulletDamage);
      }
    }
    #endregion
  }
}