﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.Core.Pawn
{
  [RequireComponent(typeof(Rigidbody))]
  public class TankPawn: Pawn
  {
    #region Private Members
    // Так как у нас нет конфига и модели не подтягиваются динамично закидываем руками через инспектор
    [SerializeField]
    private GameObject[] _turrets;
    [SerializeField]
    private GameObject _bulletPrefab;
    [SerializeField]
    private GameObject _bulletSpawnPoint;
    [SerializeField]
    private GameObject _tankRender;
    [SerializeField]
    private GameObject _bustedTank;
    [SerializeField]
    private ParticleSystem _tankExplosion;
    private int _lifeCount = 3;
    private float _rotateSpeed = 0;
    private int _defence = 0;
    private int _selectedWeapon = -1;
    private int _lastWeapon = -1;
    private int[] _weaponDamage;
    private int _moveDirection = 0;
    private int _rotateDirection = 0;
    private bool _isMove = false;
    private bool _isRorate = false;
    private Rigidbody _rigidbody;
    private ParticleSystem[] _particleSystems;
    private List<BulletController> _bulletPool = new List<BulletController>();
    private bool _isDead = false;
    private Vector3 _bulletWaitPoint;
    #endregion

    #region Properties
    public int LifeCount
    {
      get { return _lifeCount; }
    }
    public bool IsDead
    {
      get { return _isDead; }
    }
    public List<BulletController> BulletPool
    {
      get { return _bulletPool; }
      set { _bulletPool = value; }
    }
    public Vector3 BulletWaitPoint
    {
      get { return _bulletWaitPoint; }
    }
    #endregion

    #region Public Methods
    public void Initialize()
    {
      if (!ErrorFinded())
      {
        ApplicationContainer.Instance.EventHolder.SetPawnMoveDirectionEvent += GetPawnMoveDirection;
        ApplicationContainer.Instance.EventHolder.SetPawnActionEvent += GetPawnAction;
        ApplicationContainer.Instance.EventHolder.ReloadPlayerHealthIndicator();
        _rigidbody = GetComponent<Rigidbody>();
        _rigidbody.isKinematic = false;
        _particleSystems = GetComponentsInChildren<ParticleSystem>();

        if (_particleSystems != null)
        {
          for (int i = 0; i < _particleSystems.Length; i++)
          {
            _particleSystems[i].Play();
          }
        }

        _bulletWaitPoint = GameObject.FindGameObjectWithTag("DeadPoint").transform.position;

        if (_bulletWaitPoint == null)
        {
          Debug.LogError(string.Format("[{0}][Initialize]_bulletWaitPoint is empty", GetType().Name));
        }
      }
    }

    public void SetPawn(int maxHealth, int defence, float moveSpeed, int rotateSpeed, int[] weaponDamage)
    {
      _currentHealth = maxHealth;
      _maxHealth = maxHealth;
      _defence = defence;
      _moveSpeed = moveSpeed;
      _rotateSpeed = rotateSpeed;
      #region Проверка соответствия кол-во weaponDamage к кол-ву _turrets
      _weaponDamage = new int[_turrets.Length];

      if (weaponDamage.Length > _turrets.Length)
      {
        for (int i = 0; i < _weaponDamage.Length; i++)
        {
          _weaponDamage[i] = weaponDamage[i];
        }
      }
      else if (weaponDamage.Length < _turrets.Length)
      {
        for (int i = 0; i < _weaponDamage.Length; i++)
        {
          _weaponDamage[i] = Random.Range(10, 101);
        }
      }
      else
      {
        _weaponDamage = weaponDamage;
      }
      #endregion
      _selectedWeapon = 0;

      for (int i = 0; i < _turrets.Length; i++)
      {
        if (i == _selectedWeapon)
        {
          _turrets[i].SetActive(true);
        }
        else
        {
          _turrets[i].SetActive(false);
        }
      }
    }

    public void TakeDamage(int damage)
    {
      if (!_isDead)
      {
        if (damage > _defence)
        {
          _currentHealth -= damage - _defence;
        }

        if (_currentHealth <= 0)
        {
          _isDead = true;
          _currentHealth = 0;
        }

        ApplicationContainer.Instance.EventHolder.ReloadPlayerHealthIndicator();

        if (_isDead)
        {
          _lifeCount--;
          ApplicationContainer.Instance.EventHolder.ChangeLifeCount();
          ParticleSystem explosion = Instantiate(_tankExplosion.gameObject).GetComponent<ParticleSystem>();
          explosion.transform.position = transform.position;
          _tankRender.SetActive(false);
          explosion.Play();
          ApplicationContainer.Instance.AudioManager.PlayeTankExplosionSount();
          ApplicationContainer.Instance.AudioManager.StopTankSound();
          Instantiate(_bustedTank).transform.position = transform.position;
          ParticleSystem.MainModule mainModule = explosion.main;
          ApplicationContainer.Instance.GameFieldController.CameraController.ChangeTrackingStatus(false);
          Destroy(explosion.gameObject, mainModule.duration);

          if (_lifeCount > 0)
          {
            ApplicationContainer.Instance.EventHolder.ShowRespawnTime();
          }
          else
          {
            ApplicationContainer.Instance.EventHolder.ShowGameOver();
          }
        }
      }
    }

    public void Respawn()
    {
      _tankRender.SetActive(true);
      _currentHealth = _maxHealth;
      _isDead = false;
      ApplicationContainer.Instance.EventHolder.ReloadPlayerHealthIndicator();
    }
    #endregion

    #region Private Methods
    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_turrets == null || _turrets.Length == 0)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_turrets is empty", GetType().Name));
        returnValue = true;
      }

      if (_bulletPrefab == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_bulletPrefab is empty", GetType().Name));
        returnValue = true;
      }

      if (_bulletSpawnPoint == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_bulletSpawnPoint is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankRender == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankRender is empty", GetType().Name));
        returnValue = true;
      }

      if (_bustedTank == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_BustedTank is empty", GetType().Name));
        returnValue = true;
      }

      if (_tankExplosion == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_tankExplosion is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }

    private void OnDisable()
    {
      _rigidbody.isKinematic = true;

      if (_particleSystems != null)
      {
        for (int i = 0; i < _particleSystems.Length; i++)
        {
          _particleSystems[i].Stop();
        }
      }
    }

    private void OnDestroy()
    {
      ApplicationContainer.Instance.EventHolder.SetPawnMoveDirectionEvent -= GetPawnMoveDirection;
      ApplicationContainer.Instance.EventHolder.SetPawnActionEvent -= GetPawnAction;
    }

    private void GetPawnMoveDirection(PawnMoveDirection direction)
    {
      if (!_isDead)
      {
        switch (direction)
        {
          case PawnMoveDirection.MoveForward:
            _moveDirection = 1;
            if (!_isMove)
            {
              _isMove = true;
              StartCoroutine("PawnMove");
            }
            break;
          case PawnMoveDirection.MoveBack:
            _moveDirection = -1;
            if (!_isMove)
            {
              _isMove = true;
              StartCoroutine("PawnMove");
            }
            break;
          case PawnMoveDirection.StopMove:
            _moveDirection = 0;
            if (_isMove)
            {
              _isMove = false;
              StopCoroutine("PawnMove");
            }
            break;
          case PawnMoveDirection.RotateLeft:
            _rotateDirection = -1;
            if (!_isRorate)
            {
              _isRorate = true;
              StartCoroutine("PawnRotate");
            }
            break;
          case PawnMoveDirection.RotateRight:
            _rotateDirection = 1;
            if (!_isRorate)
            {
              _isRorate = true;
              StartCoroutine("PawnRotate");
            }
            break;
          case PawnMoveDirection.StopRotate:
            _rotateDirection = 0;
            if (_isRorate)
            {
              _isRorate = false;
              StopCoroutine("PawnRotate");
            }
            break;
        }
      }
    }

    private IEnumerator PawnMove()
    {
      while (true)
      {
        Vector3 movement = transform.forward * _moveDirection * _moveSpeed * Time.deltaTime;
        _rigidbody.MovePosition(_rigidbody.position + movement);
        yield return null;
      }
    }

    private IEnumerator PawnRotate()
    {
      while (true)
      {
        float turn = _rotateDirection * _rotateSpeed * Time.deltaTime;
        Quaternion turnRotation = Quaternion.Euler(0f, turn, 0f);
        _rigidbody.MoveRotation(_rigidbody.rotation * turnRotation);
        yield return null;
      }
    }

    private void GetPawnAction(PawnAction action)
    {
      if (!_isDead)
      {
        switch (action)
        {
          case PawnAction.Shoot:
            DoShot();
            break;
          case PawnAction.NextWeapon:
          case PawnAction.PreviousWeapon:
            ChangeWeapon(action);
            break;
        }
      }
    }

    private void ChangeWeapon(PawnAction action)
    {
      _lastWeapon = _selectedWeapon;

      switch (action)
      {
        case PawnAction.NextWeapon:
          if (_selectedWeapon < _weaponDamage.Length - 1)
          {
            _selectedWeapon++;
          }
          else
          {
            _selectedWeapon = 0;
          }
          break;
        case PawnAction.PreviousWeapon:
          if (_selectedWeapon > 0)
          {
            _selectedWeapon--;
          }
          else
          {
            _selectedWeapon = _weaponDamage.Length - 1;
          }
          break;
      }

      _turrets[_lastWeapon].SetActive(false);
      _turrets[_selectedWeapon].SetActive(true);
    }

    private void DoShot()
    {
      BulletController bulletController;

      if (_bulletPool.Count == 0)
      {
        bulletController = Instantiate(_bulletPrefab).GetComponent<BulletController>();
      }
      else
      {
        bulletController = _bulletPool[0];
        _bulletPool.RemoveAt(0);
      }

      bulletController.gameObject.transform.position = _bulletSpawnPoint.transform.position;
      bulletController.gameObject.transform.rotation = _bulletSpawnPoint.transform.rotation;
      bulletController.gameObject.SetActive(true);
      bulletController.Initialize(_weaponDamage[_selectedWeapon]);
    }
    #endregion
  }
}