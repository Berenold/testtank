﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.Core.Pawn
{
  [RequireComponent(typeof(NavMeshAgent))]
  public class EnemyPawn: Pawn
  {
    #region Private Members
    [SerializeField]
    private GameObject _enemyRender;
    private int _damage;
    private NavMeshAgent _navMeshAgent;
    private Transform _target;
    private Vector3 _deadPoint;
    private float _kickTimeShift = 1;
    #endregion

    #region Public Methods
    public void Initialize()
    {
      if (!ErrorFinded())
      {
        _navMeshAgent = GetComponent<NavMeshAgent>();
      }
    }

    public void SetPawn(Transform target, int damage, int maxHEalth, Material material, Vector3 deadPoint)
    {
      _target = target;
      _damage = damage;
      _maxHealth = maxHEalth;
      _currentHealth = _maxHealth;
      _enemyRender.GetComponent<Renderer>().material = material;
      _deadPoint = deadPoint;
    }

    public void TakeDamage(int damage)
    {
      _currentHealth -= damage;

      if (_currentHealth <= 0)
      {
        ChangePawnMoveStatus(false);
        _enemyRender.SetActive(false);
        transform.position = _deadPoint;
        _navMeshAgent.SetDestination(transform.position);
        ApplicationContainer.Instance.GameFieldController.EnemyPool.Add(this);
        ApplicationContainer.Instance.GameFieldController.AddScore(1);
        ApplicationContainer.Instance.GameFieldController.RespawnEnemy();
      }
    }

    public void ChangePawnMoveStatus(bool move)
    {
      if (_target != null)
      {
        if (move)
        {
          StartCoroutine("Moving");
        }
        else
        {
          StopCoroutine("Moving");
        }
      }
    }

    public void Respawn()
    {
      _currentHealth = _maxHealth;
      _enemyRender.SetActive(true);
      ChangePawnMoveStatus(true);
    }
    #endregion

    #region Private Methods
    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_enemyRender == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_enemyRender is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }

    private IEnumerator Moving()
    {
      while (true)
      {
        _navMeshAgent.SetDestination(_target.position);
        yield return null;
      }
    }

    private void OnTriggerEnter(Collider collider)
    {
      TankPawn tankPawn = collider.GetComponent<TankPawn>();

      if (tankPawn != null)
      {
        _navMeshAgent.SetDestination(transform.position);
        ChangePawnMoveStatus(false);
      }
    }

    private void OnTriggerStay(Collider collider)
    {
      TankPawn tankPawn = collider.GetComponent<TankPawn>();

      if (tankPawn != null)
      {
        _kickTimeShift -= Time.deltaTime;

        if (_kickTimeShift <= 0)
        {
          tankPawn.TakeDamage(_damage);
          _kickTimeShift = 1;
        }
      }
    }

    private void OnTriggerExit(Collider collider)
    {
      Debug.Log("OnTriggerExit");
      ChangePawnMoveStatus(true);
    }
    #endregion
  }
}