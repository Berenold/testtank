﻿using UnityEngine;

namespace Assets.Content.Scripts.Core.Pawn
{
  public class Pawn: MonoBehaviour
  {
    protected int _currentHealth = 0;
    protected int _maxHealth = 0;
    protected float _moveSpeed = 0;

    public int CurrentHealth
    {
      get { return _currentHealth; }
      set { _currentHealth = value; }
    }
    public int MaxHealth
    {
      get { return _maxHealth; }
    }
    public float MoveSpeed
    {
      get { return _moveSpeed; }
    }
  }
}