﻿using UnityEngine;

namespace Assets.Content.Scripts.Core.Pawn
{
  [System.Serializable]
  public class EnemyEssence
  {
    #region Private Members
    [SerializeField]
    private int _maxHealth;
    [SerializeField]
    private int _damage;
    [SerializeField]
    private Material _material;
    #endregion

    #region Properties
    public int MaxHealth
    {
      get { return _maxHealth; }
    }
    public int Damage
    {
      get { return _damage; }
    }
    public Material Material
    {
      get { return _material; }
    }
    #endregion

    #region Constructs
    public EnemyEssence(): this(0, 0, null){}
    public EnemyEssence(int maxHealth, int damage, Material material)
    {
      _maxHealth = maxHealth;
      _damage = damage;
      _material = material;
    }
    #endregion
  }
}