namespace Assets.Content.Scripts.Core.Pawn
{
  public enum PawnAction
  {
    PreviousWeapon,
    NextWeapon,
    Shoot,
    Default
  }
}