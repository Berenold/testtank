﻿using UnityEngine;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.UI
{
  public class MainMenuPanelController : MonoBehaviour, iPanel
  {
    #region Public Methods
    public void Initialize(object panelParams) { }

    public void StartGame()
    {
      ApplicationContainer.Instance.LoadLevel(LevelName.GameScene);
    }

    public void OpenOptionPanel()
    {

    }
    #endregion
  }
}