﻿namespace Assets.Content.Scripts.UI
{
  public interface iPanel
  {
    void Initialize(object panelParams);
  }
}