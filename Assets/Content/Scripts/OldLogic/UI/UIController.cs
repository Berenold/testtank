﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.UI
{
  public class UIController : MonoBehaviour
  {
    #region Private Members
    [SerializeField]
    private List<PanelEssence> _panels;
    private List<OpenPanel> _openPanels = new List<OpenPanel>();
    #endregion

    #region Public Methods
    public void Initialize()
    {
      DontDestroyOnLoad(gameObject);
      ApplicationContainer.Instance.EventHolder.ShowPanelEvent += ShowPanel;
      ApplicationContainer.Instance.EventHolder.HidePanelEvent += HidePanel;
      ShowPanel(PanelType.MainMenu);
    }
    #endregion

    #region Private Methods
    private void ShowPanel(PanelType currentPanel)
    {
      var panelEssence = _panels.FirstOrDefault(element => element.Type == currentPanel);

      if (panelEssence == null)
      {
        return;
      }
      
      if (!panelEssence.Panel.activeSelf)
      {
        panelEssence.Panel.SetActive(true);
        panelEssence.Panel.GetComponent<iPanel>().Initialize(null);
      }

      if (_openPanels.FirstOrDefault(element => element.Type == currentPanel) == null)
      {
        _openPanels.Add(new OpenPanel(_panels.IndexOf(panelEssence), currentPanel));
      }
    }

    private void HidePanel()
    {
      if (_openPanels.Count <= 0)
      {
        return;
      }

      _panels[_openPanels[_openPanels.Count - 1].Index].Panel.SetActive(false);
      _openPanels.RemoveAt(_openPanels.Count - 1);

      if (_openPanels.Count > 0)
      {
        ShowPanel(_openPanels[_openPanels.Count].Type);
      }
    }
    #endregion
  }
}