﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.UI
{
  public class GamePanelController : MonoBehaviour, iPanel
  {
    #region Private Members
    [SerializeField]
    private Image _progressBar;
    [SerializeField]
    private Image[] _indicators;
    [SerializeField]
    private Text _timeText;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private GameObject _messageText;
    [SerializeField]
    private GameObject _mainMenuButton;
    private Core.Pawn.TankPawn _tankPawn;
    #endregion

    #region Public Methods
    public void Initialize(object panelParams)
    {
      ApplicationContainer.Instance.EventHolder.ReloadPlayerHealthIndicatorEvent += ReloadPlayerHealthIndicator;
      ApplicationContainer.Instance.EventHolder.ShowRespawnTimeEvent += ShowRespawnTime;
      ApplicationContainer.Instance.EventHolder.ChangeLifeCountEvent += ChangeLifeCount;
      ApplicationContainer.Instance.EventHolder.ShowGameOverEvent += ShowGameOver;
      ApplicationContainer.Instance.EventHolder.UpdateScoreEvent += UpdateScore;
      _tankPawn = ApplicationContainer.Instance.GameFieldController.TankPawn;

      if (!ErrorFinded())
      {
        _messageText.SetActive(false);
        _mainMenuButton.SetActive(false);

        for (int i = 0; i < _indicators.Length; i++)
        {
          _indicators[i].gameObject.SetActive(true);
        }
      }
    }

    public void GoMainMenu()
    {
      ApplicationContainer.Instance.LoadLevel(LevelName.MainMenu);
    }
    #endregion

    #region Private Methods
    private void UpdateScore()
    {
      _scoreText.text = ApplicationContainer.Instance.GameFieldController.Score.ToString();
    }

    private bool ErrorFinded()
    {
      bool returnValue = false;

      if (_progressBar == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_progressBar is empty", GetType().Name));
        returnValue = true;
      }

      if (_indicators == null || _indicators.Length == 0)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_indicators is empty", GetType().Name));
        returnValue = true;
      }

      if (_timeText == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_timeText is empty", GetType().Name));
        returnValue = true;
      }

      if (_scoreText == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_scoreText is empty", GetType().Name));
        returnValue = true;
      }

      if (_messageText == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_messageText is empty", GetType().Name));
        returnValue = true;
      }

      if (_mainMenuButton == null)
      {
        Debug.LogError(string.Format("[{0}][ErrorFinded]_mainMenuButton is empty", GetType().Name));
        returnValue = true;
      }

      return returnValue;
    }

    private void ChangeLifeCount()
    {
      for (int i = _indicators.Length - 1; i > _tankPawn.LifeCount - 1; i--)
      {
        _indicators[i].gameObject.SetActive(false);
      }
    }

    private void ShowGameOver()
    {
      _messageText.SetActive(true);
      _mainMenuButton.SetActive(true);
    }

    private void ReloadPlayerHealthIndicator()
    {
      if (!ErrorFinded())
      {
        _progressBar.fillAmount = (float)_tankPawn.CurrentHealth / (float)_tankPawn.MaxHealth;
      }
    }

    private void ShowRespawnTime()
    {
      if (!ErrorFinded())
      {
        _timeText.gameObject.SetActive(true);
        StartCoroutine("RespawnTime");
      }
    }

    private IEnumerator RespawnTime()
    {
      float timeShift = 4f;

      while (true)
      {
        timeShift -= Time.deltaTime;
        _timeText.text = string.Format("{0}", (int)timeShift);

        if (timeShift <= 1)
        {
          ApplicationContainer.Instance.GameFieldController.RespawnTankPawn();
          _timeText.gameObject.SetActive(false);
          StopCoroutine("RespawnTime");
        }

        yield return null;
      }
    }

    private void OnDisable()
    {
      ApplicationContainer.Instance.EventHolder.ReloadPlayerHealthIndicatorEvent -= ReloadPlayerHealthIndicator;
      ApplicationContainer.Instance.EventHolder.ShowRespawnTimeEvent -= ShowRespawnTime;
      ApplicationContainer.Instance.EventHolder.ChangeLifeCountEvent -= ChangeLifeCount;
      ApplicationContainer.Instance.EventHolder.ShowGameOverEvent -= ShowGameOver;
    }
    #endregion
  }
}