﻿namespace Assets.Content.Scripts.UI
{
  public class OpenPanel
  {
    #region Properties
    public int Index { get; private set; }
    public PanelType Type { get; private set; }
    #endregion

    #region Constructs
    public OpenPanel():this(-1, PanelType.Default){}
    public OpenPanel(int index, PanelType type)
    {
      Index = index;
      Type = type;
    }
    #endregion
  }
}