﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Assets.Content.Scripts.SystemLogick;

namespace Assets.Content.Scripts.UI
{
  public class PreloaderPanelController : MonoBehaviour, iPanel
  {
    #region Private Members
    [SerializeField]
    private Image _progressBar;
    [SerializeField]
    private float _progressSpeed;
    private float _currentProgress = 0;
    #endregion

    #region Public Methods
    public void Initialize(object panelParams)
    {
      if (_progressBar != null)
      {
        _progressBar.fillAmount = _currentProgress;
        StartCoroutine(ProgressAnimation());
      }
      else
      {
        Debug.LogError(string.Format("[{0}][Initialize]_progressBar is empty", GetType().Name));
      }
    }
    #endregion

    #region Private Methods
    private IEnumerator ProgressAnimation()
    {
      bool exit = false;

      while (!exit)
      {
        if (_currentProgress < 1)
        {
          _currentProgress += Time.deltaTime * _progressSpeed;
          _progressBar.fillAmount = _currentProgress;
        }
        else
        {
          _currentProgress = 0;
          exit = true;
        }

        yield return null;
      }

      ApplicationContainer.Instance.IsPreloaderAnimationFinished = true;
      StopCoroutine(ProgressAnimation());
      ApplicationContainer.Instance.EventHolder.SetPawnMoveDirection(Core.Pawn.PawnMoveDirection.StopMove);
    }
    #endregion
  }
}