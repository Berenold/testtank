﻿using UnityEngine;

namespace Assets.Content.Scripts.UI
{
  [System.Serializable]
  public class PanelEssence
  {
    #region Private Members
    [SerializeField]
    private GameObject _panel;
    [SerializeField]
    private PanelType _type;
    #endregion

    #region Properties
    public GameObject Panel
    {
      get { return _panel; }
    }
    public PanelType Type
    {
      get { return _type; }
    }
    #endregion

    #region Constructs
    public PanelEssence():this(null, PanelType.Default){}
    public PanelEssence(GameObject _panel, PanelType _type)
    {
      _panel = null;
      _type = PanelType.Default;
    }
    #endregion
  }
}