﻿namespace Assets.Content.Scripts.UI
{
  public enum PanelType
  {
    MainMenu,
    GamePanel,
    Preloader,
    Default
  }
}